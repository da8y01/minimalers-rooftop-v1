﻿# metachatbot.ps1
# 2023-01-21

param(
    [switch]$Start = $false,
    [switch]$Stop = $false
)

$scriptArgs = ""
$appPath = "C:\repos_20220317-2024_Win11\original-coast-clothing"

try {
    if ($Start) {
        $scriptArgs = '-Start'
        Set-Location $appPath
        "START NODE APPLICATION"
        # Invoke-Expression "npm start"
    }
    if ($Stop) {
        $scriptArgs = '-Stop'
        "STOP NODE APPLICATION"
    }
    if (!$Start -AND !$Stop) {
        Invoke-WebRequest -Uri http://localhost:3000/status
    }
}
catch {
    "[ERROR] There was an unhandled exception."
}
