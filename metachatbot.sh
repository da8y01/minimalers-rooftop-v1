#!/bin/bash

if [[ $1 == "status" ]]; then
    curl https://minimalers-roof.top/cgi-bin/MetaBotStatus.sh
fi
if [[ $1 == "start" ]]; then
    curl https://minimalers-roof.top/cgi-bin/MetaBotStart.sh
fi
if [[ $1 == "stop" ]]; then
    curl https://minimalers-roof.top/cgi-bin/MetaBotStop.sh
fi
